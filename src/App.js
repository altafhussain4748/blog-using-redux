import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import React, { useState } from "react";
import Navbar from "./components/layouts/Navbar";
import PostList from "./components/PostList";
import CreatePost from "./components/CreatePost";
import { Provider } from "react-redux";
import store from './redux/store'

function App() {
  const [user] = useState({
    id: 1,
    name: "Altaf Hussain",
    userName: "Altaf.Hussain",
  });
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Navbar />
          <Switch>
            <Route
              exact
              path="/"
              render={(props) => <PostList {...props} user={user} />}
              user={user}
            />
            <Route
              exact
              path="/create-post"
              render={(props) => <CreatePost {...props} user={user} />}
            />
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
