import { POST_URL } from "./../../constants";
import {
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
} from "./postTypes";

export const fetchPosts = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchPostsRequest());
      const response = await fetch(POST_URL);
      const posts = await response.json();
      dispatch(fetchPostsSuccess(posts));
    } catch (err) {
      dispatch(fetchPostsFailure(err.message));
    }
  };
};

export const fetchPostsRequest = () => {
  return {
    type: FETCH_POSTS_REQUEST,
  };
};

export const fetchPostsSuccess = (posts) => {
  return {
    type: FETCH_POSTS_SUCCESS,
    payload: posts,
  };
};

export const fetchPostsFailure = (error) => {
  return {
    type: FETCH_POSTS_FAILURE,
    payload: error,
  };
};
