import { createSelector } from "reselect";

export const getPostsBySelector = (state) => {
  return state.post;
};

// reselect function
export const getPostState = () => {
  return createSelector([getPostsBySelector], (post) => post);
};
