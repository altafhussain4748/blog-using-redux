import React, { useState, useEffect } from "react";
import "./../App.css";
import LoaderComponent from "./Loader";
import { getPostState } from "./../redux/selectors";
import { connect } from "react-redux";
import { fetchPosts } from "../redux";

function PostList({ postData, user, fetchPosts }) {
  // Use hook to load data
  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div className="post-container">
      {postData.isLoading && (
        <div className="loading">
          <LoaderComponent />
        </div>
      )}
      {postData.error && <p className="error">Error while loading posts</p>}
      <div className="posts-section">
        {postData.posts.length > 0 && (
          <div className="recentPost">
            <p>Top 10 posts.</p>
          </div>
        )}
        {postData.posts.map((post) => {
          return (
            <div key={post.id} className="single-item">
              <div className="post-top">
                <p className="post-title">{post.title}</p>
                <p className="post-user">{user.name}</p>
              </div>
              <p className="post-description">{post.body}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
const makeMapStateToProps = () => {
  const makeGetPosts = getPostState();
  return (state) => {
    return {
      postData: makeGetPosts(state),
    };
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPosts: () => dispatch(fetchPosts()),
  };
};

export default connect(makeMapStateToProps, mapDispatchToProps)(PostList);
